﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Movies.Models;
using NToastNotify;

namespace Movies.Controllers
{
    public class MoviesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IToastNotification _notification;
        public MoviesController(ApplicationDbContext context, IToastNotification toastNotification)
        {
            _context = context;
            _notification = toastNotification;

        }
        public async Task<IActionResult> Index()
        {
            var model = await _context.Movies.OrderByDescending(m => m.Rate).ToListAsync();
            return View(model);
        }
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return BadRequest();
            var model = await _context.Movies.Include(m => m.Genre).SingleOrDefaultAsync(m => m.Id == id);
            if (model == null)
                return NotFound();
            return View(model);
        }

        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
                return BadRequest();
            var model = await _context.Movies.FindAsync(id);
            if (model == null)
                return NotFound();

            _context.Movies.Remove(model);
            _context.SaveChanges();
            return Ok();
        }
    }
}
