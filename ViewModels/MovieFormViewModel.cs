﻿using System.ComponentModel.DataAnnotations;

namespace Movies.ViewModels
{
    public class MovieFormViewModel
    {
        public int Id { get; set; }
        [Required, MaxLength(255)]
        public string Title { get; set; }
        public int Year { get; set; }
        [Range(1, 10)]
        public double Rate { get; set; }
        [Required, StringLength(2500)]
        public string StoryLine { get; set; }
        [Display(Name = "Select Poster...")]
        public byte[] Poster { get; set; }
        [Display(Name = "Genre")]
        public byte GenreId { get; set; }
        public IEnumerable<Models.Genre> Genres { get; set; }
    }
}
