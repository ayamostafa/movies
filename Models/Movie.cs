﻿using System.ComponentModel.DataAnnotations;

namespace Movies.Models
{
    public class Movie
    {
        //public Movie(Movie m)
        //{
        //    Title = m.Title;
        //    Year = m.Year;
        //    Rate = m.Rate;
        //    Storeline = m.Storeline;
        //    Poster = m.Poster;
        //    GenreId = m.GenreId;
        //}
        public int Id { get; set; }
        [Required, MaxLength(255)]
        public string Title { get; set; }
        public int Year { get; set; }

        public double Rate { get; set; }
        [Required, MaxLength(2500)]
        public string Storeline { get; set; }
        [Required]
        public byte[] Poster { get; set; }

        public byte GenreId { get; set; }
        public Genre? Genre { get; set; }
    }
}
